# workshop PrepostPrint

PrePostPrint is a two days open workshop dedicated to the experimentation of alternative publishing systems.

## Notes en vrac
Nous proposons une pièce/ outil qui montre les différents versions d'une édition crée à plusieurs.
-Faire des édition issu des doubles pages
-Chaque publication modifié est imprimé.
-Peut être couverture en riso soit 1 master par double page A3
-feuille volante avec explication du projets
-pdf imposition à la fin  +double imposé
-propager l'url accessible
-création d'un objet qui permet de comprendre le versionning.


## L'édition de base ouvert A3
Une double page par contenu autour d'um même thème
8 // une double page en couverture.
donc 8 pages =  couv + 6 pages (3 double page) // 4 feuilles


3 édition témoin de la performance ( départ-moitié-arrivé )
3 couleurs (couleur )
formulaire à chaque fois il modifie la page



## Matériel
1 Laser
Master Riso A3 x6
1 ordi


## contenu/ thème
Colaboratif
création sous condition
transmission
représentation
visualisation de donné

Un «pad» est un texte collaboratif créé à partir d'un éditeur de texte collaboratif en ligne.
Le contenu peut être l'explication de l'outil et des possibilité du print alternatif. (assez littéral)

thème du monstres transformation  évolution modification

Thème du monstre "Le modèle type d’une machine, d’un dispositif."

monstrum \Prononciation ?\ neutre
(Religion) Avertissement des dieux (constitué par un phénomène qui sort de l’ordre naturel), présage (divin).
Monstre, chose étrange, hideuse.
Fléau, malheur, crime.
Prodige, merveille, chose incroyable.

Slender Man ou Slenderman (de l'anglais « Homme élancé ») est un personnage de fiction issu d'un mème Internet, créé sur le forum Something Awful (en) par Victor Surge en 2009. Il est rapidement devenu à son tour un mème Internet et a été repris dans de nombreuses créations.


Lors des premiers temps du réseau, les usagers devaient se contenter de télécharger passivement du contenu sur leur ordinateur personnel, bercés par les stridulations du modem, mais très vite sont apparus les premiers outils permettant, avec quelques rudiments de programmation en HTML, de créer sa page personnelle, d'y afficher du texte, des images ou de la musique. C'était au siècle dernier, le Web était un foutoir de pages persos criardes et bordéliques, truffées d'ascenceurs avec fonds d'écrans cosmiques, boutons clignotants et fichiers Midi. Le Web, alors, «était chatoyant, riche, personnel, lent et en construction, résume Olia Lialina, dans un passionnant essai sur le web vernaculaire. Les pages étaient construites dans l'espoir du lendemain, de connexions meilleures et d'ordinateurs plus puissants. Ce Web des indigènes ou des barbares était un Web d'amateurs qui allaient bientôt être dégagé par les ambitieux dotcom, les outils professionnels et le design des experts».
Des experts qui jugeaient cette production triviale et de mauvais goût et ont imposé leurs normes et standards en réaction justement à ce folklore. Ce n'était pas le cas des artistes de net.art. Formé au milieu des années 90, ce mouvement basé en Europe et en Europe de l'Est (avec Olia Lialina, Alexei Shulgin, Vuk Cosic ou Heath bunting) s'est mis à collecter ces expressions populaires dans des cabinets de curiosités et à exposer cet «art trouvé » sur le web (2) parmi leurs propres créations.


Une strate (anglais : layer) est un des points du vue du joueur sur la partie en cours. Le terme vient du fait que l'on considère sept points de vue qui sont plus ou moins proches de l'immersion dans l'histoire et le monde fictionnel d'un côté, et du monde réel de l'autre. Le point de vue du joueur passe en général d'une strate à une strate voisine.

Detournement
detournement des usage
detournement dans le Web

## Texte
Workshop by Étienne Ozeray & Romain Marula

DEViation is an Print tool in the web navigator proposed for prepostprint.  
Use-it pads and give an preview in real time of a double page. Doesn't work only Chrome or Chromium.  


DEViation est un outil de mise en page de documents imprimés collaboratif dans le navigateur web. Il utilise des pads sur lesquels il est possible d'éditer le css à partir http://prepostprint.org/deviation et permet une prévisualisation en temps réel de la double-page. Chaque participant est invité à modifier, s'approprier la mise en page existante.
L'objectif de ces deux jours est d'utiliser cet outil pour produire une édition rendant compte de l'évolution de la mise en page au fil du temps et des utilisateurs. Nous proposons de dévier de la linéarité propre à l'édition imprimée en créant une série de livre comprenant l'ensemble des modifications apportées sur chaque double-page.
Ne fonctionne que sur Chrome ou Chromium.

DEViation is a collaborative printable layout tool in the web browser. It uses pads where everybody can edit css by going to http://prepostprint.org/deviation and allows a real-time previsualisation. Each participant is invited to modify and appropriate the existing layout and let it as it is for the next one.
The goal is to use this tool to product a book putting light on the evolution of the layout though time and users. We propose to deviate from the classical linearity of the book object to create a set of books where we can find each modifications to the original layout.
Works properly only on Chromium or Chrome.
http://prepostprint.org/deviation
https://gitlab.com/atelier-bek/prepostprint

![screenshot](screenshot/1.png)


## TxT shema
 --> selected a page
 --> edit the css in the right pad to create your layout
 --> Write your name, modification and email adress
 --> Pdf generation
 --> Print for the installation
 --> Print Book



##Todo
- formulaire
-- écrire nouvelle ligne dans fichier texte
- script export css
-- Gabarit: laisser div vide et insérer contenu avec content: '' dans css  
--- formulaire  
--- date et heure  
--- pagination: régler dans pad setup, c'est plus simple.  
-- récupérer contenu chaque css et mettre pages à la suite  
- trouver contenu
- rentrer contenu dans pads html
- pad html lecture seule
- trouver nom
- écrire txt
- insert
-- text explicatif  
-- changelog (fichier texte datas.txt)  
- proposer liste fontes
- pad doc bug
-~~pagination hover~~
- text how to
- ~~définir format A3
