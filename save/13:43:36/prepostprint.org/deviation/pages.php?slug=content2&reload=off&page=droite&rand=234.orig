  <!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>
			prepostprint - content2		</title>

		<link rel="stylesheet" type="text/css" media="screen" href="../css/page.css"><link rel="stylesheet" type="text/css" media="screen, print" href="css/page.css">		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	</head>
  <body>

      <style>
      @media print, screen {
                
body {
    background-color: brown;
    }
    
    a {
        color: DarkCyan ;
        }

1.h2 {
	width: 4cm; 
	transform: rotate(10deg);
	margin-top: 1cm;
	margin-left: 1cm;
	font-size: 70px;
	font-family: 'Acme', sans-serif;
}

span {
    color: white;
	font-size: 70px;
	font-family: 'Acme', sans-serif;
    }


.thumbcaption{
    width: 8cm;
    position: absolute;
    z-index: -10;
    top: 8cm;
    left: 1cm;
    transform: rotate(5deg);
	font-family: 'Acme', sans-serif;
	font-size: 3em;
    }
    
    .thumbinner {
        transform: rotate (10deg);
        size: 60px;

        }

img{
    position: fixed;

    }

p{
	width: 13cm;
	margin-left: 6.5cm; 
	color: white;
	  font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
	font-size: 0.8em;
}

h3 {
    transform: rotate (35deg);
    color: white;
    font-size: 3em;
	font-family: 'Acme', sans-serif;
    }

span #Post-tour_years{
    font-family:'Acme', sans-serif;
}
 
 img {
	border: 5px black solid;
  }   

img:hover {
    border-radius: 10%;
*}
 
    
    
    
      }
    </style>

    
 
                                        <div class="tsingle">
                                                <div class="thumbimage">
                                                        <a class="image" href="/wiki/File:Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg"><img alt="" data-file-height="474" data-file-width="300" height="275" src="//upload.wikimedia.org/wikipedia/en/thumb/6/6e/Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg/174px-Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg" srcset="//upload.wikimedia.org/wikipedia/en/thumb/6/6e/Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg/261px-Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg 1.5x, //upload.wikimedia.org/wikipedia/en/6/6e/Marcel_Duchamp_Mona_Lisa_LHOOQ.jpg 2x" width="174"></a>
                                                </div>
                                        </div>
 
 
                                        <div class="thumbcaption">
                                                At left, a wood engraving faithful to Leonardo's original, by <a href="/wiki/Timothy_Cole" title="Timothy Cole">Timothy Cole</a> (1914). At right, <em><a href="/wiki/L.H.O.O.Q." title="L.H.O.O.Q.">MONA YEAH UH</a></em> by <a href="/wiki/Marcel_Duchamp" title="Marcel Duchamp">Marcel Duchamp</a> (1919)
                                        </div>
                                </div>
                        </div>
 <p><a href="/wiki/Marcel_Duchamp" title="Marcel Duchamp">Marcel Duchamp</a>, among the most influential artists of his generation, in 1919 may have inadvertently set the standard for modern manifestations of <em>Mona Lisa</em> simply by adding a <a href="/wiki/Goatee" title="Goatee">goatee</a> to an existing postcard print of Leonardo's original. Duchamp pioneered the concept of <a href="/wiki/Readymades_of_Marcel_Duchamp" title="Readymades of Marcel Duchamp"><em>readymades</em></a>, which involves taking mundane objects not generally considered to be art and transforming them artistically, sometimes by simply renaming them and placing them in a gallery setting. In <em>L.H.O.O.Q.</em> the "found object" is a <em>Mona Lisa</em> postcard onto which Duchamp drew a goatee in pencil and appended the title.</p>
 
 
                        <p>The title, Duchamp is said to have admitted in his later years, is a pun. The letters L-H-O-O-Q pronounced in French form the sentence <em>Elle a chaud au cul</em>, <a href="/wiki/Colloquialism" title="Colloquialism">colloquially</a> translating into English as "She has a hot ass."<sup class="reference" id="cite_ref-Seekamp_LHOOQ_19-0"><a href="#cite_note-Seekamp_LHOOQ-19">[19]</a></sup> As was the case with many of his readymades, Duchamp made multiple versions of <em>L.H.O.O.Q.</em> in varying sizes and media throughout his career. An unmodified black and white reproduction of <em>Mona Lisa</em> on a playing-card, onto which Duchamp in 1965 inscribed <em>LHOOQ ras&eacute;e</em> (<em>LHOOQ Shaved</em>), is among many <em>second-generation</em> variants referencing the original <em>L.H.O.O.Q</em>.<sup class="reference" id="cite_ref-LHOOQ_20-0"><a href="#cite_note-LHOOQ-20">[20]</a></sup></p>
 
 
                        <p>Duchamp's parody of <em>Mona Lisa</em> was itself parodied by <a href="/wiki/Francis_Picabia" title="Francis Picabia">Francis Picabia</a> in 1942, annotated <em>Tableau Dada Par Marcel Duchamp</em> ("Dadaist Scene for Marcel Duchamp"),<sup class="reference" id="cite_ref-Spaces_Between_21-0"><a href="#cite_note-Spaces_Between-21">[21]</a></sup> another example of <em>second-generation</em> interpretations of <em>Mona Lisa</em>. <a href="/wiki/Salvador_Dal%C3%AD" title="Salvador Dal&iacute;">Salvador Dal&iacute;</a> created his <em>Self Portrait as Mona Lisa</em> in 1954, referencing <em>L.H.O.O.Q.</em> in collaboration with <a href="/wiki/Philippe_Halsman" title="Philippe Halsman">Philippe Halsman</a>, incorporating his photographs of a wild-eyed Dal&iacute; showing his <a href="/wiki/Handlebar_moustache" title="Handlebar moustache">handlebar moustache</a> and a handful of coins.<sup class="reference" id="cite_ref-Oxford_book_2-5"><a href="#cite_note-Oxford_book-2">[2]</a></sup><sup class="reference" id="cite_ref-Spaces_Between_21-1"><a href="#cite_note-Spaces_Between-21">[21]</a></sup><sup class="reference" id="cite_ref-Dali_Mona_22-0"><a href="#cite_note-Dali_Mona-22">[22]</a></sup> In 1958, <a href="/wiki/Iceland" title="Iceland">Icelandic</a> painter <a href="/wiki/Err%C3%B3" title="Err&oacute;">Err&oacute;</a> then incorporated Dal&iacute;'s version into a composition which also included a film-still from Dal&iacute;'s <em><a href="/wiki/Un_Chien_Andalou" title="Un Chien Andalou">Un Chien Andalou</a></em>. <a href="/wiki/Fernand_L%C3%A9ger" title="Fernand L&eacute;ger">Fernand L&eacute;ger</a> and <a href="/wiki/Ren%C3%A9_Magritte" title="Ren&eacute; Magritte">Ren&eacute; Magritte</a> are among the numbers of <a href="/wiki/Modern_art" title="Modern art">Modern art</a> masters who've adapted <em>Mona Lisa</em> using their own iconography.<sup class="reference" id="cite_ref-Oxford_book_2-6"><a href="#cite_note-Oxford_book-2">[2]</a></su
p> None of the parodies have tarnished <em>Mona Lisa'</em>s image; rather, they reinforce her fame.<sup class="reference" id="cite_ref-Oxford_book_2-7"><a href="#cite_note-Oxford_book-2">[2]</a></sup> Duchamp's <em>mustached</em><em>Mona Lisa</em> embellishment continues to inspire imitation. Contemporary conceptual artist <a href="/wiki/Subodh_Gupta" title="Subodh Gupta">Subodh Gupta</a> gave <em>L.H.O.O.Q.</em> three-dimensional form in his 2009 bronze sculpture <em><a href="/wiki/Et_tu,_Brute%3F" title="Et tu, Brute?">Et tu</a>, Duchamp?</em> Gupta, from India, considers himself an "idol thief" and has reinterpreted a number of iconic works from European art history.<sup class="reference" id="cite_ref-Gupta_23-0"><a href="#cite_note-Gupta-23">[23]</a></sup></p>
 
 
                        <h2><span class="mw-headline" id="Post-tour_years">Post-tour years (1962&ndash;2000)</span>
                        </h2>
 
 
                        <p>Radio personality <a href="/wiki/Bruce_Morrow" title="Bruce Morrow"><em>Cousin Brucie</em> Morrow</a> presided over a promotional event during <em>Mona Lisa'</em>s exhibition in New York City. 70,000 entries of a "Best Mona" painting contest were exhibited at the <a href="/wiki/Polo_Grounds" title="Polo Grounds">Polo Grounds</a>, with Salvador Dal&iacute; helping to pick the winner.<sup class="reference" id="cite_ref-Billboard_24-0"><a href="#cite_note-Billboard-24">[24]</a></sup> Following the painting's first American presentation, <a href="/wiki/Andy_Warhol" title="Andy Warhol">Andy Warhol</a> in 1963 created multiple renditions in his screen-print <a href="/wiki/Pop_art" title="Pop art">Pop art</a> style. <sup class="reference" id="cite_ref-25"><a href="#cite_note-25">[25]</a></sup> "<em>Mona Lisa</em> (Two Times)," "Four <em>Mona Lisa</em>s," and "Thirty Are Better Than One" illustrate Warhol's method of silkscreening an image repetitively within the same work of art. Also in 1963, <a href="/wiki/Fernando_Botero" title="Fernando Botero">Fernando Botero</a>&nbsp;&ndash; who had already painted "<em>Mona Lisa</em>, Age Twelve" in 1959&nbsp;&ndash; painted another, this time in what would become his trademark "Boterismo" style of rendering figures disproportionately <em>plump</em>. <em>Mona Lisa</em> is also referenced in artwork by <a href="/wiki/Contemporary_art" title="Contemporary art">Contemporary art</a> luminaries such as <a href="/wiki/Jasper_Johns" title="Jasper Johns">Jasper Johns</a> and <a href="/wiki/Robert_Rauschenberg" title="Robert Rauschenberg">Robert Rauschenberg</a>, adding to the veritable "<a href="/wiki/Who%27s_Who_in_American_Art" title="Who's Who in American Art">who's who</a>" list of artists putting their own <a class="mw-redirect" href="/wiki/Spin_(public_relations)" title="Spin (public relations)">spin</a> on the portrait.<sup class="reference" id="cite_ref-Oxford_book_2-8"><a href="#cite_note-Oxford_book-2">[
2]</a></sup></p>
 


  </body>
</html>
