#!/bin/bash


#!/bin/sh
while [ true ]
do

  hour=$(date +"%T")
  interval=300
  url=http://prepostprint.org/deviation/getPages.php

  tput setaf 1
    echo
    echo "Hi, it is $hour, I'm starting saving files"
  tput sgr0

  mkdir $hour
  cd $hour

  wget -E -H -k -K -p $url

  sed -i "/<div class='gabarit'>/a $hour" prepostprint.org/deviation/getPages.php.html

  cd ..

  tput setaf 1
    echo "Job done, I'm sleeping for $interval seconds, good night."
    sleep $interval
  tput sgr0

done
