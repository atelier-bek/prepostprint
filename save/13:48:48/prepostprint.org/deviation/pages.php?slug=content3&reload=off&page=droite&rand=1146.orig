  <!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>
			prepostprint - content3		</title>

		<link rel="stylesheet" type="text/css" media="screen" href="../css/page.css"><link rel="stylesheet" type="text/css" media="screen, print" href="css/page.css">		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	</head>
  <body>

      <style>
      @media print, screen {
                
body{
 
   font-family: 'Rock Salt', cursive;
    padding: 0cm 0;
    border-left: 10px solid darkblue; 


h2{
   font-family: 'Rock Salt', cursive;
    font-size:2em;
    text-transform: uppercase;
    border-bottom: 4px dashed red;
    padding: 0.5cm;
       -moz-box-shadow: 200px 10px 5px 0px #cb0000;
-webkit-box-shadow: 200px 10px 5px 0px #cb0000;
-o-box-shadow: 200px 10px 5px 0px #cb0000;
box-shadow: 200px 10px 5px 0px #cb0000;
filter:progid:DXImageTransform.Microsoft.Shadow(color=#cb0000, Direction=92, Strength=5);
}
}

h3{
    font-size:1.5em;
	font-family: 'Rock Salt', cursive;
    text-transform: uppercase;
    border: 8px solid white;
    display: inline-block;
    padding: 0.5cm;
}

.mw-headline {
	font-family: 'Rock Salt', cursive;
    }

h4{
    padding-left:7cm;   
}

h4:before{
    content: "//////";

    padding-right: 0.5cm;
    }

img{ 
    /*float:left;*/
    position: absolute;
    z-index: -1;
     filter: grayscale(1);
     -webkit-filter: grayscale(1);
     width: 100%;
     height: auto;
}

p{
    -webkit-column-count: 6; /* Chrome, Safari, Opera */
    -moz-column-count: 6; /* Firefox */
    column-count: 6;
    font-family:monospace;
    padding: 0 1cm;
    font-size: 0.5em;
    color: white;
    }

#Unconventional_interpretations {
    display: none;
    }


.txtleft {
    color: white;
    background-color: black;
	padding: 13cm;
    }

a{
    color:inherit;
    text-transform:uppercase;
    }

      }
    </style>

    


                        <p class="txtleft">In a film called <em><a href="/wiki/Elf_(film)" title="Elf (film)">Elf</a></em>, Buddy uses an <a href="/wiki/Etch_A_Sketch" title="Etch A Sketch">Etch-a-Sketch</a> to draw the Mona Lisa in process to build Santa Land by the North Pole in <a href="/wiki/Gimbels" title="Gimbels">Gimbels</a>. In <em><a href="/wiki/Horton_Hears_a_Who!_(film)" title="Horton Hears a Who! (film)">Horton Hears A Who</a></em>, the Mayor Ned McDodd shows his only son Jojo a family gallery where in one part his great grandmother is parodied as the Mona Lisa. And in <em><a href="/wiki/My_Little_Pony:_Equestria_Girls_%E2%80%93_Friendship_Games" title="My Little Pony: Equestria Girls &ndash; Friendship Games">My Little Pony: Equestria Girls &ndash; Friendship Games</a></em>, there is a cake that Pinkie Pie and Fluttershy have baked with a picture of the Mona Lisa inside.</p>


                        <h3><span class="mw-headline" id="Unconventional_interpretations">Unconventional interpretations</span>
                        </h3>


                        <p class="txtleft"><em>Mona Lisa</em> replicas are sometimes directly or indirectly embellished as commentary of contemporary events. Exhibitions or events with ties to Leonardo da Vinci or Renaissance art also provide an opportunity for local artists to exploit <em>Mona Lisa'</em>s image toward promoting the events.<sup class="reference" id="cite_ref-Lego_30-1"><a href="#cite_note-Lego-30">[30]</a></sup> The resulting artworks represent a broad spectrum of artists using <a href="/wiki/Artistic_license" title="Artistic license">creative license</a>.</p>


                        <p class="txtleft">In 2009, a replica of <em>Mona Lisa</em> was pieced-together using precious gemstones by a jewelry collector in China. Using approximately 100,000 carats of multi-colored jewels amassed over 30 years, the replica required five years to complete. The resulting artwork was publicly displayed at a <a class="mw-redirect" href="/wiki/Shenyang_City" title="Shenyang City">Shenyang City</a> shopping center.<sup class="reference" id="cite_ref-Mona_of_Jewels_33-0"><a href="#cite_note-Mona_of_Jewels-33">[33]</a></sup> In a similar vein, artist Kristen Cumings in 2010 created her own "Jelly Bean Mona" replica using over 10,000 jelly beans. The one initial creation led to a full series of eight masterpiece replicas commissioned by a California jelly bean company as a <a href="/wiki/Publicity_stunt" title="Publicity stunt">publicity stunt</a> and addition to the company's collection. <a href="/wiki/Ohio" title="Ohio">Ohio</a>'s <a class="mw-redirect" href="/wiki/Center_of_Science_and_Industry" title="Center of Science and Industry">Center of Science and Industry</a> (COSI) in <a href="/wiki/Columbus,_Ohio" title="Columbus, Ohio">Columbus</a> thought the series noteworthy enough to be featured in an exhibition, held at the end of 2012.<sup class="reference" id="cite_ref-jellybean_34-0"><a href="#cite_note-jellybean-34">[34]</a></sup></p>


                        <p class="txtleft">A replica of <em>Mona Lisa</em> publicized as the "<em>world's smallest</em>" was painted by Andrew Nichols of <a href="/wiki/New_Hampshire" title="New Hampshire">New Hampshire</a> (USA) in 2011, intending "to break the record." Recreated at a 70:1 ratio, the miniature <em>Mona Lisa</em> measures approximately 1/4 by 7/16 inches (7 by 11&nbsp;mm). Although his rendition drew media attention, it was never officially reported whether he had, in fact, broken any existing record.<sup class="noprint Inline-Template Template-Fact">[<em><a href="/wiki/Wikipedia:Citation_needed" title="Wikipedia:Citation needed"><span title="This claim needs references to reliable sources. (August 2013)">citation needed</span></a></em>]</sup> In 2013, a far smaller version of the painting, entitled the <em><a href="/wiki/Mini_Lisa" title="Mini Lisa">Mini Lisa</a></em>, was created by a <a href="/wiki/Georgia_Institute_of_Technology" title="Georgia Institute of Technology">Georgia Institute of Technology</a> student named Keith Carroll. The replica was created to demonstrate a new scientific technique called <a href="/wiki/Thermochemical_nanolithography" title="Thermochemical nanolithography">thermochemical nanolithography</a> (TCNL). The <em>Mini Lisa</em> was just 30 micrometres (0.0012&nbsp;in) wide, about 1/25,000th the size of the original.<sup class="reference" id="cite_ref-35"><a href="#cite_note-35">[35]</a></sup></p>


                        <p class="txtleft">High school students attracted media attention in 2011 by recreating <em>Mona Lisa</em> on <a href="/wiki/Daytona_Beach,_Florida" title="Daytona Beach, Florida">Daytona Beach, Florida</a> (USA), using seaweed which had accumulated on shore. Claiming to have "too much time on their hands," it took two people approximately one hour to "turn the ugly seaweed into a work of art." Aside from photos appearing in the press, presumably their efforts were washed away with the tide.<sup class="reference" id="cite_ref-seaweed_36-0"><a href="#cite_note-seaweed-36">[36]</a></sup></p>


                        <p class="txtleft">In 2012 the Portuguese designer Lu&iacute;s Silva created a poster for a campaign against violence on women representing Mona Lisa with a sore eye and a sombre expression, with the slogan "Could you live without her smile?".<sup class="reference" id="cite_ref-37"><a href="#cite_note-37">[37]</a></sup></p>


                        <h4><span class="mw-headline" id="Mosaics">Mosaics</span>
                        </h4>


                        <p class="txtleft">The computer age introduced digitally-produced or -inspired incarnations of <em>Mona Lisa</em>. Aside from versions constructed of actual computer <a href="/wiki/Motherboard" title="Motherboard">motherboards</a>,<sup class="reference" id="cite_ref-motherboards_38-0"><a href="#cite_note-motherboards-38">[38]</a></sup><a href="/wiki/Mosaic" title="Mosaic">mosaic</a>-making techniques are another common motif used in such re-creations.<sup class="reference" id="cite_ref-jellybean_34-1"><a href="#cite_note-jellybean-34">[34]</a></sup></p>


                        <p class="txtleft">A <a href="/wiki/Photographic_mosaic" title="Photographic mosaic">photo mosaic</a> of <em>Mona Lisa</em> was digitally produced in 2012 from randomly compiled photos using <em>adaptive rendering software</em>,<sup class="reference" id="cite_ref-photo_mosaic_39-0"><a href="#cite_note-photo_mosaic-39">[39]</a></sup> to promote the potential of <em>Simultaneous Multi-compare Adaptive Rendering Technology</em> (<em>SMART</em>), which automatically analyzes and matches the shapes and colors of source-photos to a desired image.<sup class="reference" id="cite_ref-mosaic_soft_40-0"><a href="#cite_note-mosaic_soft-40">[40]</a></sup></p>


                        <p class="txtleft">Mimicking the heavy <a href="/wiki/Pixelation" title="Pixelation">pixelation</a> of a highly magnified computer file, <a href="/wiki/Canada" title="Canada">Canadian</a> artist Robert McKinnon assembled 315 <a href="/wiki/Rubik%27s_Cube" title="Rubik's Cube">Rubik's Cubes</a> into a 36 by 48 inch <em>Mona Lisa</em> mosaic, an effect dubbed "Rubik's Cubism" by <a href="/wiki/France" title="France">French</a> artist <a href="/wiki/Invader_(artist)" title="Invader (artist)">Invader</a>.<sup class="reference" id="cite_ref-Rubik_41-0"><a href="#cite_note-Rubik-41">[41]</a></sup> Similarly, colored <a href="/wiki/Lego" title="Lego">Lego</a><em>bricks</em> have been employed to replicate <em>Mona Lisa</em> in a mosaic motif. A 2011 exhibition titled <em>Da Vinci, The Genius</em> at the <a class="mw-redirect" href="/wiki/Frazier_Museum" title="Frazier Museum">Frazier Museum</a> in <a href="/wiki/Louisville,_Kentucky" title="Louisville, Kentucky">Louisville, Kentucky</a> attracted attention by having a <em>Mona Lisa</em> constructed by <a href="/wiki/Lego_in_popular_culture#Art" title="Lego in popular culture">Lego artist</a> Brian Korte.<sup class="reference" id="cite_ref-Lego_30-2"><a href="#cite_note-Lego-30">[30]</a></sup> Known as <em>Brick Art</em>, so-called "pro" Lego builders such as <a href="/wiki/Eric_Harshbarger" title="Eric Harshbarger">Eric Harshbarger</a> have made multiple replicas of Mona Lisa. Matching the approximate 21 by 30 inch size (535 x 760+ mm) of Leonardo's original<sup class="reference" id="cite_ref-mona_THEFT_5-1"><a href="#cite_note-mona_THEFT-5">[5]</a></sup> requires upwards of 5,000 standard Lego <em>bricks</em>, but replicas measuring 6 by 8 feet have been built, requiring more than 30,000 bricks.<sup class="reference" id="cite_ref-Lego2_42-0"><a href="#cite_note-Lego2-42">[42]</a></sup><sup class="reference" id="cite_ref-Lego3_43-0"><a href="#cite_note-Lego3-43">[43]</a></sup><sup class="reference" i
d="cite_ref-Leg
o4_44-0"><a href="#cite_note-Lego4-44">[44]</a></sup></p>


                        <p class="txtleft">At a 2009 festival in <a class="mw-redirect" href="/wiki/Sydney,_Australia" title="Sydney, Australia">Sydney, Australia</a>, a group of eight people spent three hours creating a <em>Mona Lisa</em> replica from 3,604 cups of coffee. The mosaic effect in this case was created by adding amounts of milk, or sometimes none, to cups of black coffee. The final product, measuring 20 by 13 feet, was dubbed the "<a class="mw-redirect" href="/wiki/Cafe_mocha" title="Cafe mocha"><em>Mocha</em></a> Lisa."<sup class="reference" id="cite_ref-Mocha_Lisa_1_45-0"><a href="#cite_note-Mocha_Lisa_1-45">[45]</a></sup><sup class="reference" id="cite_ref-Mocha_Lisa_2_46-0"><a href="#cite_note-Mocha_Lisa_2-46">[46]</a></sup> Maurice "Toastman" Bennet, known for recreating masterpieces using toasted bread, parodied <em>Mona Lisa</em> in a toast mosaic. At a 2010 food-art festival in <a href="/wiki/Hong_Kong" title="Hong Kong">Hong Kong</a>, Bennet <em><a href="/wiki/Tile" title="Tile">tiled</a></em> together approximately 6,000 slices of bread, using a <a class="mw-redirect" href="/wiki/Blow-torch" title="Blow-torch">blow-torch</a> to achieve the desired <em>tone</em> on each piece of toast.<sup class="reference" id="cite_ref-burnt_toast_Mona_47-0"><a href="#cite_note-burnt_toast_Mona-47">[47]</a></sup></p>



  </body>
</html>
